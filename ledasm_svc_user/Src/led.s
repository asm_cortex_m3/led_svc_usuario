		.syntax		unified
		.cpu		cortex-m0
		.thumb

/****************************************/
/*	Defino variables en RAM				*/
/****************************************/
	.equ LEN_PILA,	512
	.section	.bss
	.comm	pila_sin_privilegios, LEN_PILA



/****************************************/
/*	Defino constantes de programa		*/
/****************************************/
.equ PORTC_ODR,		0x4001100C	// .equ es similar a  #define
.equ GPIOC_CHR, 	0x40011004	// Puerto GPIOC
.equ RCC_APB2ENR, 	0x40021018	// Registros para habilitar el clock de GPIOC
.equ LOOP_COMPARE, 	0x2ffff		// Demora por software.

		.section	.text
		.align		2
 		.global		main, SVC_Handler

/****************************************/
/*	Función main. Acá salta boot.s 		*/
/*	cuando termina.						*/
/*	main NO TIENE QUE TERMINAR NUNCA	*/
/****************************************/
		.type		main, %function
main:
		//Inicializo el pin como salida
		BL		led_init
		LDR		R0,=(pila_sin_privilegios + LEN_PILA)	//Genero una pila para el PSP
		MSR		PSP,R0									//cargo el PSP

		MRS		R0,CONTROL								//Leo el registro de control
		MOVS	R1,#3									//Me paso al modo usuario y uso el PSP como pila
		ORRS	R0,R1									//Hago la OR
		MSR		CONTROL,R0								//Cargo R0 en CONTROL (hago algunas demás para usar MRS)

main_loop:

		SVC		1										//La Service Call 1 enciende el led.
		LDR		R0,=#LOOP_COMPARE						//
		BL		delay									//espero.
		SVC		0										//SVC 0 apaga el led.
		LDR		R0,=#LOOP_COMPARE*8						//espero*8
		BL		delay									//
		B		main_loop								//reinicio el lazo.


/****************************************/
/*	Función led_init. 				 	*/
/*	Inicializa El LED					*/
/****************************************/
		.type	delay, %function
led_init:
		PUSH	{R1, R2, LR}		// Mando a la pila los registros que modifico y LR
		LDR		R1, =(1 << 4)       // Cargo en R1 el bit que me habilita el GPIOC
		LDR 	R2, =#RCC_APB2ENR   // Cargo la dirección de memoria
		STR		R1, [R2]            // Habilito la señal de reloj para GPIOC
									//Pongo GPIOC13 como salida.
		LDR 	R1, =(0b11 << 20)
		LDR 	R2, =#GPIOC_CHR
		STR 	R1, [R2]
		POP		{R1, R2, PC}

/****************************************/
/*	Función led_set. 				 	*/
/*	Setea el led en funcion de R0		*/
/****************************************/
		.type	delay, %function
led_set:
		PUSH	{R0, R1, R2}		// Mando a la pila todos los registros que modifico
		MVNS	R0,R0				// R0   = ~R0
		MOVS	R1,#1				// R1   = 0x01
		ANDS	R0,R0,R1			// R0 & = 0x01
		LSLS	R0,R0,#13			// R0 <<= 13
		ldr 	R2, =#PORTC_ODR   	// Escribo la dirección de memoria para setear GPIOC
		STR 	R0, [R2]          	// Escribo el puerto de salida
		POP		{R0, R1, R2}		// Repongo los registros que toqué.
		BX		LR

/****************************************/
/*	Service CALL de ejemplo. 			*/
/*  Con 1 enciende led, con 0 apaga		*/
/****************************************/
.type		SVC_Handler, %function
SVC_Handler:
		MOV		R3,LR				//Mando el LR a R3
		MRS		R2,MSP				//MSP a R2
		MOV	 	R0,LR				//El bit 2 de LR en 1 me dice que la pila activa es PSP
		MOVS	R1,#4				//Preparo máscara
		ANDS 	R0,R1				//Enmascaro
		BEQ		SVC_parametro		//Si Z = 1 salto -> LR[2]=0 -> pila activa MSP
		MRS		R2,PSP				//cargo PSP en R2
SVC_parametro:
		LDR		R1,=#24				//Me desplazo hasta el PC del stacking
		LDR		R0,[R2,R1]			//Me traigo el PC al que tengo que volver del stacking.
		SUBS	R0,#2				//Voy una instrucción para atrás (Thumb2 -> 16bits), es la SVC
		LDR		R0,[R0]				//Leo la instrucción SVC
		MOVS	R1,#255				//Preparo máscara
		ANDS	R0,R1				//Enmascaro y en R0 dejo el parámetro de ls SVC
		BL		led_set				//activo el led en función del valor de R0
		BX 		R3					//salgo de la Excepción con el LR original (lo pisé en la línea anterior)
									//No pusheo R0, R1, R2 ni R3 porque se mandaron a la pila en la excepción.


/****************************************/
/*	Función delay. 				 		*/
/*	Recibe por R0 la demora				*/
/****************************************/
		.type	delay, %function
delay:
		PUSH	{R0, LR}			// Guardo el parámetro y LR en la pila.
delay_dec:
        SUBS	R0, 1				//
        BNE		delay_dec			// while(--R0);
		POP		{R0, PC}			// repongo R0 y vuelvo.
.end
